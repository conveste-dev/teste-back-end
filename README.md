****Sobre a Conveste**** 

A Conveste tem como objetivo oferecer serviços especializados aos players dos mercados financeiro e imobiliário, proporcionando através de suas atividades, segurança e credibilidade das informações que podem ser apuradas por meio de auditoria e gerenciamento de ativos imobiliários de acordo com as exigências do mercado.

****Teste Back End .Net Core C#****

O objetivo deste teste é entendermos um pouco mais sobre seus conhecimentos Back End em .Net Core com C#.

****Requisitos****
- .Net Core
- C#
- SQL Server

****Orientações****
- Faça um fork deste projeto.
- A validação da WebApi será feita utilizando-se softwares para teste de apis Rest, Insomnia ou Postman;
- O tráfego de dados de entrada e saída da WebApi deverá estar no formato de Objetos Json;

****O Desafio****
- Desenvolver uma WebApi para simular o cadastro de uma propriedade e criar um contrato para a mesma.

__Funcionalidade 1:__
- Permitir o cadastro de um imóvel com algumas características.
- O cadastro de um imóvel deve possuir: E-mail do proprietário, Rua, Número, Complemento, Bairro, Cidade, Estado;
- Para que o cadastro ocorra deverá haver validações no backend: e-mail, rua, bairro, cidade e estado são campos obrigatórios;

__Funcionalidade 2:__
- Permitir visualização dos imóveis cadastrados;

__Funcionalidade 3:__
- Permitir a remoção de uma propriedade;

__Funcionalidade 4:__
- Permitir atualização de dados de uma propriedade;

__Funcionalidade 5:__
- Criação de um contrato que permita associação com uma propriedade. Um contrato possui os seguintes campos: Propriedade, Tipo de pessoa (Pessoa física ou Pessoa Jurídica), Documento (Aqui será informado o número de um CPF para Pessoa física, ou CNPJ para Pessoa Jurídica), E-mail do contratante, Nome completo do contratante, Data do Contrato;
- Regras específicas sobre a criação de contrato:
- Uma propriedade não pode estar associada a dois contratos;
- Todos os campos do contrato são obrigatórios;
- Deverá ocorrer validação do documento;

__Funcionalidade 6:__
- Permitir a listagem dos contratos, contendo: Propriedade, E-mail do Proprietário, Nome completo do contratante, E-mail do Contratante;


****Entrega****
- Deixar um repositório público e nos enviar por e-mail - o mesmo e-mail que foi enviado o teste.
- Enviar instruções como podemos subir a WebApi em ambiente local;
- Enviar script para criação do banco de dados;
- O teste deverá ser entregue no prazo de 48 horas após o recebimento do e-mail solicitando o teste;

